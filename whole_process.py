# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.11.3
#   kernelspec:
#     display_name: nlp
#     language: python
#     name: nlp
# ---

import generate_ip
import check_logs
from random import randrange

# #### Generowanie listy ip logow

directory = 'data'

filename_logs = 'logs.txt'

amount_ip_logs = 100000

list_ips_logs = generate_ip.generate_list_ips(amount_ip_logs)

# Zamienianie pewnych ip, żeby na pewno się powtarzały

list_repeating = generate_ip.generate_list_ips(50)

for r in list_repeating:
    list_ips_logs[randrange(amount_ip_logs)] = r



# Zapis do pliku

generate_ip.write_to_file(directory, filename_logs, list_ips_logs)



# #### Generowanie listy niebezpiecznych ip

directory = 'data'

filename_malicious = 'malicious_ip.txt'

amount_ip_malicious = 3000

malicious_ips = generate_ip.generate_list_ips(amount_ip_malicious)

# Zamienianie pewnych ip, żeby na pewno się powtarzały

for r in list_repeating:
    malicious_ips[randrange(amount_ip_malicious)] = r

# Zapis do pliku

generate_ip.write_to_file(directory, filename_malicious, malicious_ips)



# ### Ładownie pliku logów

directory = 'data'

filename_logs = 'logs.txt'

list_logs_ip = check_logs.open_ip_file(directory, filename_logs)



# ### Ładownie plik z niebezpiecznymi ip

directory = 'data'

filename_malicious = 'malicious_ip.txt'

list_malicious_ip = check_logs.open_ip_file(directory, filename_malicious)

filename_warninigs = 'logs_warnings.txt'



# %%timeit
check_logs.basic_set_loop(list_logs_ip, list_malicious_ip, directory, filename_warninigs)

# %%timeit
check_logs.basic_list_loop(list_logs_ip, list_malicious_ip, directory, filename_warninigs)



# Jak widzimy wersja zamieniająca listę na zbiór jest dużo szybsza. Wynika to z tego, że wyszukiwanie w zbiorze jest dużo szybsze. W zbiorze x in s ma średnią złożoność obliczeniową O(1) a w liście O(n), co powoduje bardzo dużą różnicę.


