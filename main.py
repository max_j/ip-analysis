# Ładowanie własnych funkcji
import generate_ip
import check_logs

# Wczytywanie katalogu i nazwy plików od użytkownika
print("Proszę podać ścieżkę do katalogo z plikiem z logami")
directory = input()

print("Proszę podać nazwę pliku z logami")
filename_logs = input()

print("Proszę podać nazwę pliku z niebezpiecznymi IP")
filename_malicious = input()

print("Proszę podać nazwę pliku docelowego")
filename_warnings = input()

# Wszytywanie danych z plików do list
list_malicious_ip = check_logs.open_ip_file(directory, filename_logs)
list_logs_ip = check_logs.open_ip_file(directory, filename_malicious)


#
check_logs.basic_set_loop(list_logs_ip, list_malicious_ip, directory, filename_warnings)