from pathlib import Path
from random import randrange

# generowanie ciągu znaków - string - będącego poprawnym ip
def generate_ip_string():
    whole_ip = ''
    for i in range(4):
        random_octet = randrange(256)
        if i < 3:
            whole_ip = whole_ip + str(random_octet) + '.'
        else:
            whole_ip = whole_ip + str(random_octet)
    return whole_ip

# generowanie listy ip
def generate_list_ips(amount):
    list_ips = []
    for j in range(amount):
        generated_ip = generate_ip_string()
        # dodanie znaku końca linii
        generated_ip = generated_ip + '\n'
        list_ips.append(generated_ip)

    return list_ips

# Zapisanie listy do pliku
def write_to_file(directory, filename, list_):

    data_folder = Path(directory)
    file_to_open = data_folder / filename
    file_ = open(file_to_open, 'w')
    file_.writelines(list_)
    file_.close()

