# IP analysis

W pliku generate_ip.py zawierają się funkcje generujące pliki z adresami IP.

W pliku check_logs.py są funcje do analizowania logów.

basic_set_loop() to implementacja szybsza ze zbiorem (niebezpieszne IP po wczytaniu do listy, lista jest konwertowana na zbiór).

basic_list_loop() to implementacja z dwoma listami.

Jak widzimy wersja zamieniająca listę na zbiór jest dużo szybsza. Wynika to z tego, że wyszukiwanie w zbiorze jest dużo szybsze. W zbiorze x in s ma średnią złożoność obliczeniową O(1) a w liście O(n), co powoduje bardzo dużą różnicę.

Im plik z niebezpiecznymi IP jest większy tym bardziej widać różnicę.