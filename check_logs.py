from pathlib import Path

# otwieranie pliku z adresami ip
def open_ip_file(directory, filename):

    data_folder = Path(directory)
    file_to_open = data_folder / filename
    file = open(file_to_open)
    list_ips = file.read().splitlines()
    file.close()
    return list_ips


# funkcja sprawdzająca czy ip z jednej listy występuje w drugiej liście
# wersja z zamiana listy na zbiór, badziej optymalna
def basic_set_loop(log_ips, malicious_ips, directory, filename):

    data_folder = Path(directory)
    file_to_open = data_folder / filename
    file = open(file_to_open, 'w')

    # konwersja listy na zbior, co przyscpiesza pozniejsze wyszukiwanie
    set_malicious_ips = set(malicious_ips)
    for ip in log_ips:
        if ip in set_malicious_ips:
            print("warning", ip)
            file.write('warning: ')
            file.write(ip)
            file.write('\n')
        else:
            file.write(ip)
            file.write('\n')
    file.close()


# funkcja sprawdzająca czy ip z jednej listy występuje w drugiej liście
# wersja bez zamiany listy na zbiór, wolniejsza
def basic_list_loop(log_ips, malicious_ips, directory, filename):

    data_folder = Path(directory)
    file_to_open = data_folder / filename
    file = open(file_to_open, 'w')

    for ip in log_ips:
        if ip in malicious_ips:
            print("warning", ip)
            file.write('warning: ')
            file.write(ip)
            file.write('\n')
        else:
            file.write(ip)
            file.write('\n')
    file.close()